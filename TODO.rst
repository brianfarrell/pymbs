===========================
Known Issues & Enhancements
===========================


Known Issues
------------

* Insufficient Test Coverage
* No Automated Tesing Pipeline
* Collateral cash flows are calculated `slightly` differently from other models
* Can not run collateral cash flows for Known Collateral
* Can not run collateral cash flows for Securitized Collateral
* "0 PSA" Benchmark cash flows do not use Bond Value
* Notional tranches are not implemented
* MACR tranches are not implemented
* No ability to run Residual cash flows 


Enhancement Proposals
---------------------

* Create Domain Specific Language for modeling payment rules
* Allow for queries to FRED DB for current and historic interest rate values
