
============
Introduction
============

PyMBS is a Python library for use in modeling Mortgage-Backed Securities.

    * This is a modern Python library that requires ``Python>=3.10``


Objectives
----------
#. Create a modern, stable Python library with a clearly-defined API  
#. Provide as close to 100% Test Coverage as possible [#f1]_
#. Provide clear documentation with full coverage of the API and example code.



Installing
----------

Install using `conda`:

.. code-block:: bash

   conda install -c btf pymbs


Install using `pip`:

.. code-block:: bash

   pip install pymbs


Links
-----

* Documentation: https://brianfarrell.gitlab.io/pymbs/
* License: https://www.gnu.org/licenses/agpl.html
* Anaconda Cloud: https://anaconda.org/btf/pymbs/
* PyPi Releases: https://pypi.org/project/pymbs/
* Code: https://gitlab.com/brianfarrell/pymbs

.. rubric:: Footnotes

.. [#f1] | This project started-out as a Proof of Concept (POC).
         | At that point, no automated testing was involved.
         | Going forward, all new development and bug fixes will be test-driven.
