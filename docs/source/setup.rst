.. _setup:

===========================
Set up your environment
===========================

The easiest way to start exploring PyMBS is by setting up a structured
cash flow model in Jupyter Lab.  If you're new to Jupyter Lab, rest easy -
the effort required to produce a modicum of results is relatively small.

That's not to say that Jupyter Lab is lacking in power - quite to the contrary,
but it's learning curve is gentle.  If you've heard of and/or used Jupyter
Notebooks before and you're wondering how they differ from Jupyter Lab, just
know that the Lab is the latest evolution of the Notebook.  If you're
intersted in a more in-depth discussion of the differences, take a look at
this blog post:
`Jupyter Lab: Evolution of the Jupyter Notebook <https://towardsdatascience.com/jupyter-lab-evolution-of-the-jupyter-notebook-5297cacde6b>`_


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   setup_anaconda
   setup_modeling
