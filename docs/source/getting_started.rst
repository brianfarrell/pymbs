===============
Getting Started
===============

In this alpha stage of developmnet, PyMBS is *slightly* biased towards use
with a Jupyter Notebook as its front end. However, it should be easy to
integrate into a Microservices back end. It may be possible to do this already,
but that is not yet guaranteed, as it has not been tested in such a
configuration. Further research and testing will be required before such a
claim can be made.

This project is still alpha stage development. These first iterations are to
demonstrate a Proof Of Concept (POC).

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   caveats
   setup
   run_the_model   


Beyond the information provided in this documetation, please keep in mind the
following additional resources:

**PyMBS Source Code Repository:** https://gitlab.com/brianfarrell/pymbs

**Anaconda Cloud:** https://anaconda.org/
   
**Anaconda Documentation:** https://conda.io/en/latest/
   
**Jupyter Lab Documentation:** https://jupyterlab.readthedocs.io/en/stable/index.html
