
<!DOCTYPE html>

<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" /><meta name="generator" content="Docutils 0.18.1: http://docutils.sourceforge.net/" />

    <title>Set up your Modeling Environment &#8212; PyMBS 0.3.1 documentation</title>
    <link rel="stylesheet" type="text/css" href="_static/pygments.css" />
    <link rel="stylesheet" type="text/css" href="_static/alabaster.css" />
    <script data-url_root="./" id="documentation_options" src="_static/documentation_options.js"></script>
    <script src="_static/jquery.js"></script>
    <script src="_static/underscore.js"></script>
    <script src="_static/_sphinx_javascript_frameworks_compat.js"></script>
    <script src="_static/doctools.js"></script>
    <link rel="index" title="Index" href="genindex.html" />
    <link rel="search" title="Search" href="search.html" />
    <link rel="next" title="Run the Model" href="run_the_model.html" />
    <link rel="prev" title="Set up Anaconda and Jupyter Lab" href="setup_anaconda.html" />
   
  <link rel="stylesheet" href="_static/custom.css" type="text/css" />
  
  
  <meta name="viewport" content="width=device-width, initial-scale=0.9, maximum-scale=0.9" />

  </head><body>
  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          

          <div class="body" role="main">
            
  <section id="set-up-your-modeling-environment">
<span id="setup-modeling"></span><h1>Set up your Modeling Environment<a class="headerlink" href="#set-up-your-modeling-environment" title="Permalink to this heading">¶</a></h1>
<p>In the previous section, you installed Anaconda, PyMBS, and Jupyter Lab.
Before we can make use of PyMBS though, we need a structured cash flow model
to explore.</p>
<p>Fortuantely, there is one that is already partially setup that you can clone
from a Git repository. This is the model that I have been using as my reference
for developing PyMBS.</p>
<p><strong>1. Clone the model from the GitLab repo</strong></p>
<blockquote>
<div><p>In your web browser, navigate to <a class="reference external" href="https://gitlab.com/brianfarrell/fhl2618">https://gitlab.com/brianfarrell/fhl2618</a>
This is the git repository for the cash flow model. You do not need
an account on this GitLab server in order to access this repo. When you
arrive at the URL, this is what you should see:</p>
<figure class="align-center" id="id1">
<a class="reference internal image-reference" href="_images/fhl2618_repo.jpg"><img alt="Anaconda Navigator" src="_images/fhl2618_repo.jpg" style="width: 800px; height: 464px;" /></a>
<figcaption>
<p><span class="caption-text"><em>The fhl2618 GitLab Repository</em></span><a class="headerlink" href="#id1" title="Permalink to this image">¶</a></p>
</figcaption>
</figure>
<p>In the upper-right-hand corner, click on the <code class="docutils literal notranslate"><span class="pre">Clone</span></code> button to copy the
URL that you will need to clone the repository to your machine, using Git.
The use of Git is really outside the scope of this document.  If you are
unfamiliar with Git, there are plenty of resources available online to
help you with it, including this one here:
<a class="reference external" href="https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html">https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html</a></p>
</div></blockquote>
<p><strong>2. Configure PyMBS</strong></p>
<blockquote>
<div><p>Before you can open the model in Jupyter Lab, you need to tell PyMBS where
to find it.  You can do this by specifying a project directory in a
configuration file.</p>
<p>The User is able to customize settings via the <code class="docutils literal notranslate"><span class="pre">config.yaml</span></code> file, which
is located in a subdirectory of the User’s HOME directory.</p>
<p>This exact location of this directory differs by platform, as shown below:</p>
<table class="docutils align-default">
<thead>
<tr class="row-odd"><th class="head"><p>Platform</p></th>
<th class="head"><p>Config Path</p></th>
</tr>
</thead>
<tbody>
<tr class="row-even"><td><p>Mac OSX</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">~/.config/pymbs/config.yaml</span></code></p></td>
</tr>
<tr class="row-odd"><td><p>Linux</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">~/.config/pymbs/config.yaml</span></code></p></td>
</tr>
<tr class="row-even"><td><p>Windows</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">C:\Users\&lt;user&gt;\AppData\Local\pymbs\config.yaml</span></code></p></td>
</tr>
</tbody>
</table>
<p>If desired, the configuration values may be set using environment variables,
instead of using the <code class="docutils literal notranslate"><span class="pre">config.yaml</span></code> file.  Additionally, an alternate
location for the <code class="docutils literal notranslate"><span class="pre">config.yaml</span></code> file itself may be specified in the
<strong>PYMBS_CONFIG_PATH</strong> environment variable.</p>
<p>See the <a class="reference internal" href="pymbs.config.html#config-mod"><span class="std std-ref">pymbs.config module</span></a> section of this documentation for details, including
the names of the other environment variables.</p>
<p>Below is a sample <code class="docutils literal notranslate"><span class="pre">config.yaml</span></code> file similar to the one that you
will need to prepare before attempting to open a model in Jupyter Lab.</p>
<p>The <strong>project directory</strong> key is the only value that is <strong>required</strong>.</p>
<div class="highlight-yaml notranslate"><div class="highlight"><pre><span></span><span class="nn">---</span>
<span class="nt">pymbs</span><span class="p">:</span>
<span class="w">    </span><span class="nt">project directory</span><span class="p">:</span><span class="w"> </span><span class="s">&#39;/Users/username/Projects/finance/FRE/REMICs&#39;</span>
</pre></div>
</div>
<p>You can copy and paste the code block above directly into a text file and
save it as <code class="docutils literal notranslate"><span class="pre">config.yaml</span></code> at the path noted in the table above for your
platform (or in another path, if you choose to specify one, using the
<strong>PYMBS_CONFIG_PATH</strong> environment variable).</p>
<p>Modify the path of the <code class="docutils literal notranslate"><span class="pre">project</span> <span class="pre">directory</span></code> key from that in the template
above so that it points to the correct path on your machine. The project
directory is actually the <strong>parent</strong> directory of the fhl2618 direcotry
that was created when you cloned the fhl2618 Git repoistory.</p>
<p>For illustrative purposes, suppose that you created a <code class="docutils literal notranslate"><span class="pre">cmo</span></code> directory
inside of your <code class="docutils literal notranslate"><span class="pre">HOME</span></code> directory and that you cloned the fhl2618 repo
into the <code class="docutils literal notranslate"><span class="pre">cmo</span></code> directory.</p>
<p>Your directory structure would appear as so:</p>
<div class="highlight-text notranslate"><div class="highlight"><pre><span></span>HOME
    \__cmo
          \__fhl2618
</pre></div>
</div>
<p>In this case, your <code class="docutils literal notranslate"><span class="pre">project</span> <span class="pre">directory</span></code> would be
<code class="docutils literal notranslate"><span class="pre">'/HOME/cmo'</span></code>.  You can continue to place other deals inside the
<code class="docutils literal notranslate"><span class="pre">cmo</span></code> directory and PyMBS will know how to find them.</p>
</div></blockquote>
<p><strong>3. Download a copy of the Prospectus Supplement</strong></p>
<blockquote>
<div><p>When reverse-engineering a structured cash flow model, it’s helpful to
have a copy of the Prospectus Supplement, aka “Prosupp”, or
“Offering Circular”, if one is available. In this case, one is available -
go ahead and download a copy of the prosupp for
<a class="reference external" href="https://freddiemac.mbs-securities.com/api/download/FRE/135984/2618oc">Freddie Mac REMIC Series 2618</a></p>
<p>At this point in development, PyMBS is only capable of modeling Group 3
from this deal.  The payment rules for Group 3 can be found on page 6 of
the prosupp.  The collateral cash flows can be run for <strong>all</strong> of the
groups in this deal.  In the near future, I would expect PyMBS to be
capable of handling the payment rules for Groups 1 and 4.</p>
<p>The payment rules for Group 2 of this deal are the most difficult to
handle at this time. This is due to the limited parsing ability of the
initial implementation of PyMBS.  If this framework is to be able to
handle more complex payment rules, I will need to develop a Domain
Specific Language (DSL) for the payment rules and a parser for the DSL.
This would require a level of effort that goes beyond the scope of a
Proof of Concept (POC).</p>
<p>Finally, as noted in the <a class="reference internal" href="caveats.html#caveats"><span class="std std-ref">Caveats and Limitations</span></a> section of this documentation,
PyMBS is not yet capable of paying down the balance of Notional tranches,
nor is there logic yet to handle MACR tranches (those outlined on page 45
of the prosupp). These too were effectively out of scope for a POC.</p>
<p>Now, let’s open up Jupyter Lab and <a class="reference internal" href="run_the_model.html#run-the-model"><span class="std std-ref">Run the Model</span></a>!</p>
</div></blockquote>
</section>


          </div>
          
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
<h1 class="logo"><a href="index.html">PyMBS</a></h1>








<h3>Navigation</h3>
<p class="caption" role="heading"><span class="caption-text">Contents:</span></p>
<ul class="current">
<li class="toctree-l1"><a class="reference internal" href="project.html">Introduction</a></li>
<li class="toctree-l1 current"><a class="reference internal" href="getting_started.html">Getting Started</a><ul class="current">
<li class="toctree-l2"><a class="reference internal" href="caveats.html">Caveats and Limitations</a></li>
<li class="toctree-l2 current"><a class="reference internal" href="setup.html">Set up your environment</a></li>
<li class="toctree-l2"><a class="reference internal" href="run_the_model.html">Run the Model</a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="pymbs.html">pymbs package</a></li>
<li class="toctree-l1"><a class="reference internal" href="todo.html">Known Issues &amp; Enhancements</a></li>
<li class="toctree-l1"><a class="reference internal" href="changes.html">Change Log</a></li>
</ul>

<div class="relations">
<h3>Related Topics</h3>
<ul>
  <li><a href="index.html">Documentation overview</a><ul>
  <li><a href="getting_started.html">Getting Started</a><ul>
  <li><a href="setup.html">Set up your environment</a><ul>
      <li>Previous: <a href="setup_anaconda.html" title="previous chapter">Set up Anaconda and Jupyter Lab</a></li>
      <li>Next: <a href="run_the_model.html" title="next chapter">Run the Model</a></li>
  </ul></li>
  </ul></li>
  </ul></li>
</ul>
</div>
<div id="searchbox" style="display: none" role="search">
  <h3 id="searchlabel">Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"/>
      <input type="submit" value="Go" />
    </form>
    </div>
</div>
<script>document.getElementById('searchbox').style.display = "block"</script>








        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="footer">
      &copy;2019-2023, Brian Farrell.
      
      |
      Powered by <a href="http://sphinx-doc.org/">Sphinx 5.0.2</a>
      &amp; <a href="https://github.com/bitprophet/alabaster">Alabaster 0.7.12</a>
      
      |
      <a href="_sources/setup_modeling.rst.txt"
          rel="nofollow">Page source</a>
    </div>

    

    
  </body>
</html>