
<!DOCTYPE html>

<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" /><meta name="generator" content="Docutils 0.18.1: http://docutils.sourceforge.net/" />

    <title>Set up Anaconda and Jupyter Lab &#8212; PyMBS 0.3.1 documentation</title>
    <link rel="stylesheet" type="text/css" href="_static/pygments.css" />
    <link rel="stylesheet" type="text/css" href="_static/alabaster.css" />
    <script data-url_root="./" id="documentation_options" src="_static/documentation_options.js"></script>
    <script src="_static/jquery.js"></script>
    <script src="_static/underscore.js"></script>
    <script src="_static/_sphinx_javascript_frameworks_compat.js"></script>
    <script src="_static/doctools.js"></script>
    <link rel="index" title="Index" href="genindex.html" />
    <link rel="search" title="Search" href="search.html" />
    <link rel="next" title="Set up your Modeling Environment" href="setup_modeling.html" />
    <link rel="prev" title="Set up your environment" href="setup.html" />
   
  <link rel="stylesheet" href="_static/custom.css" type="text/css" />
  
  
  <meta name="viewport" content="width=device-width, initial-scale=0.9, maximum-scale=0.9" />

  </head><body>
  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          

          <div class="body" role="main">
            
  <section id="set-up-anaconda-and-jupyter-lab">
<span id="anaconda-jupyter"></span><h1>Set up Anaconda and Jupyter Lab<a class="headerlink" href="#set-up-anaconda-and-jupyter-lab" title="Permalink to this heading">¶</a></h1>
<p>Most computer languages are supported by a plethora of 3rd-party libraries,
frameworks, and extensions.  These packages of code are in turn supported by
package managers.  With Python, there are several package managers available,
including the long used Pip, and the ascendent Pipenv.  In additon, many
platform package managers, like DNF and APT on Linux, and Homebrew on MacOS,
support the installation of a subset of Python packages.</p>
<p>In the Data Science community, the most commonly used package manager is
Anaconda.  There’s good reason for this - Anaconda supports over 1,500 Data
Science packages, written in Python and R, and optimized for delivery across
MacOS, Linux, and Windows.  It also comes with the Anaconda Navigator
Graphical User Interface, which some will find a refreshing alternative to
managing their computing environments from the Command Line Interface.  If
you prefer the CLI - don’t worry - your Anconda installation will provide that
too.  Now, let’s get started!</p>
<p><strong>1. Download Anaconda for your platform</strong></p>
<blockquote>
<div><p>Go to <a class="reference external" href="https://www.anaconda.com/download">https://www.anaconda.com/download</a> and download the appropriate
version of Anaconda for your platform.</p>
</div></blockquote>
<p><strong>2. Install Anaconda</strong></p>
<blockquote>
<div><p>On MacOS and Windows, the file you downloaded above will launch an install
wizard that will quickly step you through the install process.  As with
most things Linux, a wizard installer is not available, but if you’re
using Linux, you won’t find this install particularly taxing.  Anaconda
provides a self-extracting shell script for installation on Linux.  If you
would like to see the Anconda Documentation on the Installation process,
it is available here: <a class="reference external" href="https://docs.anaconda.com/free/anaconda/install/">https://docs.anaconda.com/free/anaconda/install/</a></p>
</div></blockquote>
<p><strong>3. Launch Anaconda Navigator and Sign-in to Anaconda Cloud</strong></p>
<blockquote>
<div><p>When you lauch Anaconda Navigator, you will be presented with a window
similar to this one, below.  Yours will probably look slightly different,
as I have been using mine during the development of of PyMBS.</p>
<figure class="align-center" id="id1">
<a class="reference internal image-reference" href="_images/anaconda_navigator.jpg"><img alt="Anaconda Navigator" src="_images/anaconda_navigator.jpg" style="width: 800px; height: 464px;" /></a>
<figcaption>
<p><span class="caption-text"><em>Anaconda Navigator</em></span><a class="headerlink" href="#id1" title="Permalink to this image">¶</a></p>
</figcaption>
</figure>
<p>In the upper-right corner, you see a button that will allow you to sign-in
to Anaconda Cloud. You don’t need to an Anaconda Cloud account to use
Jupyter and PyMBS, but sign-up is free.  If you don’t have an account,
navigate over to <a class="reference external" href="https://anaconda.org/">https://anaconda.org/</a> in your web browser and sign-up for
one.  After you’ve done that, come back to the Anaconda Navigator and
Sign-in.</p>
</div></blockquote>
<p><strong>4. Create a new environment for your modeling work</strong></p>
<blockquote>
<div><p>In Anaconda Navigator, click on the <em>Environments</em> tab in the left-hand
navigation bar.  You should see something similar to the image below.  If
this is your first time using Anaconda, you will only see the
<code class="docutils literal notranslate"><span class="pre">base</span> <span class="pre">(root)</span></code> environment.  In the picture below, you’ll see a second
one called <code class="docutils literal notranslate"><span class="pre">pymbs</span></code>, which is the one that I use for developing this
framework.</p>
<figure class="align-center" id="id2">
<a class="reference internal image-reference" href="_images/anaconda_environments.jpg"><img alt="Anaconda Environments" src="_images/anaconda_environments.jpg" style="width: 800px; height: 464px;" /></a>
<figcaption>
<p><span class="caption-text"><em>Anaconda Environments</em></span><a class="headerlink" href="#id2" title="Permalink to this image">¶</a></p>
</figcaption>
</figure>
<p>Click on the <code class="docutils literal notranslate"><span class="pre">Create</span></code> button at the bottom of the environments list and
create an environment to use for running the MBS models.  You can call it
anything you like, such as <code class="docutils literal notranslate"><span class="pre">MBS</span></code> or <code class="docutils literal notranslate"><span class="pre">cmo_modeling</span></code> or
<code class="docutils literal notranslate"><span class="pre">the_puzzle_factory</span></code> (spaces in the name are not allowed, but underscores
are).  Select <code class="docutils literal notranslate"><span class="pre">Python</span> <span class="pre">3.7</span></code> as your interpreter.  After you create this,
you will add some packages to it to help run your models.</p>
<figure class="align-center" id="id3">
<a class="reference internal image-reference" href="_images/create_new_env.jpg"><img alt="Create New Environment" src="_images/create_new_env.jpg" style="width: 480px; height: 265px;" /></a>
<figcaption>
<p><span class="caption-text"><em>Create New Environment</em></span><a class="headerlink" href="#id3" title="Permalink to this image">¶</a></p>
</figcaption>
</figure>
</div></blockquote>
<p><strong>5. Add some packages to your environment.</strong></p>
<blockquote>
<div><p>After creating your new environment, you’ll find that Anaconda went ahead
and added about 17 packages or so as part of the initial setup. These are
basic packaages that most if not all environments are going to need.
To model structured cash flows in Jupyter Lab, we’ll need to add a few
more.</p>
<p>First, let’s add the PyMBS package.  As a package manager, Anaconda will
be aware of any packages that <code class="docutils literal notranslate"><span class="pre">pymbs</span></code> is dependent upon, so adding
<code class="docutils literal notranslate"><span class="pre">pymbs</span></code> will add those packages that it depends on as well.</p>
<p>With so many packages out there, the possibility of two (or more) developers
choosing the same name for their package is not unrealistic.  In order to
handle these name collisions gracefully, Anaconda employs <em>channels</em>, aka
<em>namespaces</em>. There are two main channels included in the initial install
of Anaconda.  These channels contain most of the heavily used, well-known
packages, like OpenSSL, Pandas, and NumPy.</p>
<p>For newer, lesser known packages like <code class="docutils literal notranslate"><span class="pre">pymbs</span></code>, the developer creates their
own channel and supplies the package through that.  In order to download
<code class="docutils literal notranslate"><span class="pre">pymbs</span></code>, click on the <code class="docutils literal notranslate"><span class="pre">Channels</span></code> button in the top center of the
Anaconda Navigator window. When the new window opens, click on the
<code class="docutils literal notranslate"><span class="pre">Add</span></code> button and enter <code class="docutils literal notranslate"><span class="pre">btf</span></code>, then click the <code class="docutils literal notranslate"><span class="pre">Update</span> <span class="pre">Channels</span></code> button
to complete the addition.</p>
<figure class="align-center" id="id4">
<a class="reference internal image-reference" href="_images/add_channel.jpg"><img alt="Create New Environment" src="_images/add_channel.jpg" style="width: 465px; height: 334px;" /></a>
<figcaption>
<p><span class="caption-text"><em>Add the btf Channel</em></span><a class="headerlink" href="#id4" title="Permalink to this image">¶</a></p>
</figcaption>
</figure>
<p>Now that you’ve added the <code class="docutils literal notranslate"><span class="pre">btf</span></code> channel, Anaconda has access to the
<code class="docutils literal notranslate"><span class="pre">pymbs</span></code> package.</p>
<p>In the pull-down menu to the left of the <code class="docutils literal notranslate"><span class="pre">Channels</span></code> button, select
<code class="docutils literal notranslate"><span class="pre">Not</span> <span class="pre">installed</span></code>.  In the <code class="docutils literal notranslate"><span class="pre">Search</span> <span class="pre">Packages</span></code> field on the right,
search for <code class="docutils literal notranslate"><span class="pre">pymbs</span></code>.  When <code class="docutils literal notranslate"><span class="pre">pymbs</span></code> shows-up in the results, check
the box on the left side of its row and click on the <code class="docutils literal notranslate"><span class="pre">Apply</span></code> button,
in the lower-right-hand corner of the window.</p>
<figure class="align-center" id="id5">
<a class="reference internal image-reference" href="_images/install_pymbs.jpg"><img alt="Install PyMBS" src="_images/install_pymbs.jpg" style="width: 800px; height: 464px;" /></a>
<figcaption>
<p><span class="caption-text"><em>Install PyMBS</em></span><a class="headerlink" href="#id5" title="Permalink to this image">¶</a></p>
</figcaption>
</figure>
<p>Some machinations will ensue, as Anaconda determines the dependencies for
<code class="docutils literal notranslate"><span class="pre">pymbs</span></code> that will also need to be installed.  In the end, you will be
presented with a list of the packages to be installed.  Click <code class="docutils literal notranslate"><span class="pre">Apply</span></code>.</p>
<figure class="align-center" id="id6">
<a class="reference internal image-reference" href="_images/install_packages.jpg"><img alt="Install Packages" src="_images/install_packages.jpg" style="width: 428px; height: 409px;" /></a>
<figcaption>
<p><span class="caption-text"><em>Install Packages</em></span><a class="headerlink" href="#id6" title="Permalink to this image">¶</a></p>
</figcaption>
</figure>
<p>Even though we are using Jupyter Lab to explore PyMBS, <code class="docutils literal notranslate"><span class="pre">pymbs</span></code> is not
not actually dependent on <code class="docutils literal notranslate"><span class="pre">jupyterlab</span></code>, so <code class="docutils literal notranslate"><span class="pre">jupyterlab</span></code> is still not
installed. Go ahead and run through the steps above again that you used to
install <code class="docutils literal notranslate"><span class="pre">pymbs</span></code>, this time searching for and installing <code class="docutils literal notranslate"><span class="pre">jupyterlab</span></code>.</p>
<p>That’s it! But it’s not time to start up Jupyter Lab just yet.  In the
next section, we’ll clone the repo for the cash flow model and then things
will really start humming along…</p>
</div></blockquote>
<p><a class="reference internal" href="setup_modeling.html#setup-modeling"><span class="std std-ref">Set up your Modeling Environment</span></a></p>
</section>


          </div>
          
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
<h1 class="logo"><a href="index.html">PyMBS</a></h1>








<h3>Navigation</h3>
<p class="caption" role="heading"><span class="caption-text">Contents:</span></p>
<ul class="current">
<li class="toctree-l1"><a class="reference internal" href="project.html">Introduction</a></li>
<li class="toctree-l1 current"><a class="reference internal" href="getting_started.html">Getting Started</a><ul class="current">
<li class="toctree-l2"><a class="reference internal" href="caveats.html">Caveats and Limitations</a></li>
<li class="toctree-l2 current"><a class="reference internal" href="setup.html">Set up your environment</a></li>
<li class="toctree-l2"><a class="reference internal" href="run_the_model.html">Run the Model</a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="pymbs.html">pymbs package</a></li>
<li class="toctree-l1"><a class="reference internal" href="todo.html">Known Issues &amp; Enhancements</a></li>
<li class="toctree-l1"><a class="reference internal" href="changes.html">Change Log</a></li>
</ul>

<div class="relations">
<h3>Related Topics</h3>
<ul>
  <li><a href="index.html">Documentation overview</a><ul>
  <li><a href="getting_started.html">Getting Started</a><ul>
  <li><a href="setup.html">Set up your environment</a><ul>
      <li>Previous: <a href="setup.html" title="previous chapter">Set up your environment</a></li>
      <li>Next: <a href="setup_modeling.html" title="next chapter">Set up your Modeling Environment</a></li>
  </ul></li>
  </ul></li>
  </ul></li>
</ul>
</div>
<div id="searchbox" style="display: none" role="search">
  <h3 id="searchlabel">Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"/>
      <input type="submit" value="Go" />
    </form>
    </div>
</div>
<script>document.getElementById('searchbox').style.display = "block"</script>








        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="footer">
      &copy;2019-2023, Brian Farrell.
      
      |
      Powered by <a href="http://sphinx-doc.org/">Sphinx 5.0.2</a>
      &amp; <a href="https://github.com/bitprophet/alabaster">Alabaster 0.7.12</a>
      
      |
      <a href="_sources/setup_anaconda.rst.txt"
          rel="nofollow">Page source</a>
    </div>

    

    
  </body>
</html>