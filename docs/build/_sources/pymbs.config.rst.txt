.. _config_mod:

pymbs.config module
===================

.. automodule:: pymbs.config
   :members:
   :undoc-members:
   :show-inheritance:
